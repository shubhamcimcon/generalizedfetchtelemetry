import json
import time
import requests
import pandas as pd
from requests.structures import CaseInsensitiveDict
from collections import defaultdict

asset_devices={}
asset_device_parameters={}
telemetry_data={}
NoDeviceAssetIds=[]
NoDeviceTypeDeviceId=[]
NoDataFoundDeviceId=defaultdict(list)
NoDeviceParaDeviceId=[]

def read_config(filename):
    with open(filename)as f:
        config = json.load(f)
    return config

def get_thingsboard_token(thingsboard_ip, username, password):
    headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    }
    data = '{"username":"'+username+'", "password":"'+password+'"}'
    response = requests.post('http://'+thingsboard_ip+'/api/auth/login', headers=headers, data=data)
    token = response.json()['token']
    return token

def get_asset_id(token,thingsboard_ip,asset_type):
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["X-Authorization"] = f"bearer {token}"
    url = f"http://{thingsboard_ip}/api/user/assets?pageSize=100&page=0"
    response = requests.get(url, headers=headers)
    asset_data = response.json()['data']
    asset_ids = [asset['id']['id'] for asset in asset_data if asset['type'] in (config['asset_type']).split(",")]
    return asset_ids

def get_asset_devices(token,thingsboard_ip,asset_id):
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["X-Authorization"] = f"bearer {token}"
    url = f"http://{thingsboard_ip}/api/relations/info?fromId={asset_id}&fromType=ASSET"
    device_data = requests.get(url, headers=headers).json()
    device_ids= [device['to']['id'] for device in device_data]
    return device_ids

def device_types(token,thingsboard_ip,device_id):
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["X-Authorization"] = f"bearer {token}"
    url = f"http://{thingsboard_ip}/api/plugins/telemetry/DEVICE/{device_id}/values/timeseries?keys=Device_Type&useStrictDataTypes=false"
    resp = requests.get(url, headers=headers).json()
    device_type=resp["Device_Type"][0]["value"]
    return device_type

def get_device_parameters(token, thingsboard_ip, device_id, parameters):
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["X-Authorization"] = f"bearer {token}"
    url = f"http://{thingsboard_ip}/api/plugins/telemetry/DEVICE/{device_id}/keys/timeseries"
    device_parameters = requests.get(url, headers=headers).json()
    return list(set(parameters).intersection(device_parameters))

def get_parameter_string(list_of_parameters):
    temp = str(list_of_parameters)
    temp = temp.replace("'","")
    temp = temp.replace("{","")
    temp = temp.replace("}","")    
    temp = temp.replace("[","")
    temp = temp.replace("]","")
    temp = temp.replace(", ",",")
    return temp

def get_data(token, thingsboard_ip, device_id, parameters, interval, aggregator, startTs, endTs):
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    headers["X-Authorization"] = f"bearer {token}"
    records = 10000
    parameters = get_parameter_string(parameters)
    url = f"http://{thingsboard_ip}/api/plugins/telemetry/DEVICE/{device_id}/values/timeseries?interval={interval}&limit={records}&agg={aggregator}&orderBy=DESC&useStrictDataTypes=false&keys={parameters}&startTs={startTs}&endTs={endTs}"
    data = requests.get(url, headers=headers).json()
    return(data)

def fetch_data(thingsboard_ip, username, password, startTs, endTs, interval, aggregator,asset_type,device_type_vs,telemetry_parameters):
    token=get_thingsboard_token(thingsboard_ip, username, password)
    asset_ids=get_asset_id(token,thingsboard_ip,asset_type)
    for asset_id in asset_ids:
        device_ids_VS=[]
        device_ids=get_asset_devices(token,thingsboard_ip,asset_id)
        for device_id in device_ids:
            device_type=device_types(token,thingsboard_ip,device_id)
            if (not device_type):
                NoDeviceTypeDeviceId.append(device_id)
            elif device_type[-3:] in device_type_vs.split(","):
                device_ids_VS.append(device_id)
        asset_devices[asset_id] = device_ids_VS
    asset_devices_copy=asset_devices.copy()
    for asset_id in asset_devices.keys():    
        asset_device_parameters[asset_id] = {}
        for device_id in asset_devices[asset_id]:
            device_para= get_device_parameters(token, thingsboard_ip, device_id, telemetry_parameters)    
            if (not device_para):
                NoDeviceParaDeviceId.append(device_id)
                asset_devices_copy[asset_id].remove(device_id)
            else:
                asset_device_parameters[asset_id][device_id] =device_para
    for asset_id in asset_device_parameters.keys():
        if(not asset_device_parameters[asset_id]):
            NoDeviceAssetIds.append(asset_id)            
        else:
            telemetry_data[asset_id] = {}
            for device_id in asset_device_parameters[asset_id]:
                data= get_data(token,thingsboard_ip, device_id, asset_device_parameters[asset_id][device_id],interval,aggregator,startTs,endTs)
                if (not data):
                    NoDataFoundDeviceId[asset_id].append(device_id)
                    if len(NoDataFoundDeviceId[asset_id])==len(asset_devices_copy[asset_id]):
                        del telemetry_data[asset_id]
                else:                 
                    telemetry_data[asset_id][device_id] =data
    return telemetry_data

def json_to_dataframe(telemetry_data):
    dataframe={}
    for asset_id in telemetry_data:
        for device_id in telemetry_data[asset_id]:
            for parameter in telemetry_data[asset_id][device_id]:
                df=pd.json_normalize(telemetry_data[asset_id][device_id][parameter])
                df1=df.sort_values(by="ts").reset_index(drop=True).copy()
                dataframe[asset_id+"="+device_id+"="+parameter]=df1
    return dataframe

config=read_config("Config_Percentage_change.json")
startTs=config["startTs"]
endTs=round(time.time()*1000)
telemetry_data= fetch_data(config['thingsboard_ip'],config['tanent_username'],config['tanent_password'],startTs, endTs, config['interval'], config['aggregator'],config["asset_type"],config["device_type_vs"],config["telemetry_parameters"])
if not(telemetry_data):
    print("None Of The Devices Are Providing Data, Check startTs/endTs Or Device Telemetry From TB")
else:
    DataFrame=json_to_dataframe(telemetry_data)
    
print("No devices found for below asset ids \n",NoDeviceAssetIds)
print("\nNo device Type Found For Below Device Ids \n",NoDeviceTypeDeviceId)
print("\nNo Data Found For Below Device Ids",NoDataFoundDeviceId)
print("\nParameters Are Not Found For Below Device Ids",NoDeviceParaDeviceId)
NoDeviceAssetIds.clear()
NoDeviceTypeDeviceId.clear()
NoDataFoundDeviceId.clear()
NoDeviceParaDeviceId.clear()